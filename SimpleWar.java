// Necessary Import Commands//

import java.util.Scanner;
import java.util.Random;

// SimpleWar Class Starts//

public class SimpleWar {

    // Main Method//

    public static void main(String[] args) {

        Deck warDeck = new Deck(); // 1. New Object//

        warDeck.shuffle(); // 1.1. Shuffle//

        // 3. Players Setup//
        int playerA = 0;
        int playerB = 0;

        System.out.println(warDeck.drawTopCard());

        System.out.println(warDeck.calculateScore());
    }
}

// End//
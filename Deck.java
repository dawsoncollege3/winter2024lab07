// Necessary Import Commands//

import java.util.Scanner;
import java.util.Random;

// Deck Class Starts//

public class Deck {

    // Attributes//

    private Card[] cards;
    private int numberOfCards;
    private Random rng; // Prefer Random random//

    // Constructor//

    public Deck() {

        this.cards = new Card[52];
        this.numberOfCards = 52;
        this.rng = new Random(this.cards.length + 1);

        // Looping the cards//

        String[] suits = { "Clubs", "Diamonds", "Hearts", "Spades" }; // [] of SUITS//
        String[] values = { "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
                "Nine", "Ten", "Jack", "Queen", "King" }; // [] of VALUES//

        int deckCounter = 0;
        for (String valuesCounter : values) { // For the VALUES [13]
            for (String suitsCounter : suits) { // For the SUITS [4]
                this.cards[deckCounter] = new Card(suitsCounter, valuesCounter);
                deckCounter++;
            }
        }
    }

    public int length() {
        return this.cards.length;
    }

    public Card drawTopCard() {
        this.numberOfCards--;
        return this.cards[this.numberOfCards - 1];
    }

    public String toString() {
        String remainingDeck = " ";
        for (int a = 0; a < this.cards.length; a++) {
            remainingDeck += this.cards[a] + "\n";
        }

        return remainingDeck;
    }

    public void shuffle() {
        for (int b = 0; b < this.cards.length; b++) {
            Card swap = this.cards[b]; // newPosition = currentPosition //
            this.cards[b] = this.cards[rng.nextInt(52)]; // currentPoisiton = randomPosition//
            this.cards[rng.nextInt(52)] = swap; // randmonPosition = newPosition//
        }
    }

    public void printCards() {
        for (Card cardaa : this.cards) {
            System.out.println(cardaa);
        }
    }
}

// End//
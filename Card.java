// Necessary Import Commands//

import java.util.Scanner;
import java.util.Random;

// Card Class Starts//

public class Card {

    // Attributes//

    private String cardSuit;
    private String cardValue;

    // Constructor//

    public Card(String cardSuit, String cardValue) {
        this.cardSuit = cardSuit;
        this.cardValue = cardValue;
    }

    // Getter Methods//

    public String getCardSuit() {
        return this.cardSuit;
    }

    public String getCardValue() {
        return this.cardValue;
    }

    // toString() Method//

    public String toString() {
        return cardValue + " of " + cardSuit;
    }

    public double calculateScore() {
        for (int i = 0; i < 4; i++) {
            
        }
        return cardRank + cardSymbol;

    }

}

// End//
